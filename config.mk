# getxkblayout version
VERSION = 1.0

# Customize below to fit your system

# paths
PREFIX = /usr/local

# includes and libs
INCS = -I/usr/include 
LIBS = -lX11 -lxkbfile

CFLAGS   = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -Os ${INCS}
LDFLAGS  = ${LIBS}

# compiler and linker
CC = cc
