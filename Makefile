include config.mk

SRC = getxkblayout.c
OBJ = ${SRC:.c=.o}

all: options getxkblayout

options:
	@echo getxkblayout build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.mk

getxkblayout: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f getxkblayout ${OBJ} 

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f getxkblayout ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/getxkblayout

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/getxkblayout

.PHONY: all options clean install uninstall
